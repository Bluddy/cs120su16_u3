#include <stdio.h>

const int SIZE = 200;

void copy_arr(int dst[], int src[], int len)
{
    for (int i=0; i<=len; ++i) {
        dst[i] = src[i];
    }
}

int arr2[20];

void copy_str(char dst[], char src[])
{
    int i;
    while (src[i]) {
        dst[i] = src[i];
    }
}

void test_str()
{
    char str[] = "hello world";
    char str2[5];
    char str3[11];

    copy_str(str2, str);
    copy_str(str3, str);

    puts(str2);
    puts(str3);
}

void test_arr()
{
    int arr[] = {1, 3, 10, 2, 0};
    int arr2[SIZE];

    copy_arr(arr2, arr, sizeof(arr)/sizeof(arr[0]));

    for(int i=0; i<12; i++) {
        printf("%d ", arr2[i]);
    }
    puts("");
}

int main()
{
    test_str();
    test_arr();
}

